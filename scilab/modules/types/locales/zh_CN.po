# Chinese (Simplified) translation for scilab
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2020-08-15 16:11+0000\n"
"Last-Translator: GuoZhiqiang <763199198@qq.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 1b66c075b8638845e61f40eb9036fabeaa01f591)\n"

msgid "Double"
msgstr "双精度"

msgid "Polynomial"
msgstr "多项式"

msgid "Boolean"
msgstr "布尔"

msgid "Sparse"
msgstr "稀疏"

msgid "Boolean Sparse"
msgstr "布尔稀疏"

msgid "Matlab Sparse"
msgstr "Matlab稀疏"

msgid "Integer"
msgstr "整数"

msgid "Graphic handle"
msgstr "图像句柄"

msgid "String"
msgstr "字符串"

msgid "User function"
msgstr "用户函数"

msgid "Function library"
msgstr "函数库"

msgid "List"
msgstr "列表"

msgid "Tlist"
msgstr "Tlist"

msgid "Mlist"
msgstr "Mlist"

msgid "Struct"
msgstr "结构体"

msgid "Cell"
msgstr "Cell"

msgid "Pointer"
msgstr "指针"

msgid "Implicit polynomial"
msgstr "隐式多项式"

msgid "Intrinsic function"
msgstr "内置函数"

msgid "Unknown datatype"
msgstr "未知数据类型"
